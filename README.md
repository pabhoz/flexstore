# BabelJS

Para poder usar la sintaxis de clases de **ES6** y hacer **import**s y **export**s sin usar el old fashioned '**require**' instalaremos babel dentro de nuestro proyecto.

__¿por qué instalarlo dentro del proyecto y no global?__
1. Diferentes proyectos pueden tener distintas configuraciones y versiones de **Babel**
2. El Proyecto será más portable si todas sus dependencias están definidas en el

Para instalar **Babel** global lo haremos con:

`npm install --save-dev babel-cli`

Mientras que instalarlo para el proyecto particularmente se haría con:

`npm install --save-dev babel-cli babel-preset-env`

## Package.json

Posterior a esto si la instalación fue global, se creará un archivo [**package.json**](https://docs.npmjs.com/files/package.json) con la definicón del proyecto, si es local (como lo estamos haciendo), entonces se habrá generado un [**package-lock.json**](https://docs.npmjs.com/files/package-lock.json).

inicializaremos el gestor de dependencias de node

`npm init`

y llenamos los campos:

* Package name: co.edu.usbcali.flexstore
* version (1.0.0): (le damos enter porque esta es la versión)
* description: Taller preparcial
* entry point: src/ux.js
* test command: test
* git repository: https://bitbucket.org/pabhoz/flexstore
* keywords: flex, grid, audio, video, js, es6
* author: pabhoz & vos acá
* license: MIT

y por último **yes**

Esto nos habrá creado un archivo package.json

## Completar el Package.json

Ahora obtendremos la versión de nuestro babel CLI ubicada en el **package-lock.json** en este caso **"version": "6.26.0"** y modificamos nuestro package.json

```
{
  "name": "co.edu.usbcali.flexstore",
  "version": "1.0.0",
  "description": "Taller preparcial",
  "main": "src/ux.js",
  "dependencies": {
   "babel-cli": "^6.26.0",
   "babel-preset-env": "^1.6.0"
  },
  "devDependencies": {
+    "babel-cli": "^6.0.0"
  },
  "scripts": {
    "test": "test",
+   "build": "babel src -d lib"
  },
  ...
  
```
de esta manera hemos referenciado las dependencias de desarrollo y hemos creado un script build para que babel tome el código fuente de nuestra app y lo transpile en la carpeta **lib**.

## .babelrc

Ahora para poder activar el entorno de babel es necesario crear el archivo de configuración de babel **.babelrc**

```
{
  "presets": ["env"]
}
```

## Cambiar los scripts en index.html

Ahora sólo tendremos que cambiar nuestros scripts de **src** a **lib**

`<script src="src/ux.js"></script>`

a

`<script src="lib/ux.js"></script>`

## Ejecutar build

Ahora para transpilar el proyecto usamos el comando que creamos

`npm run-script build`

y listo, ahora sólo un problema adicional, al usar la sintaxis de clases ES6 y tratar de hacer un **require** generado por babel, el navegador arrojará un error, pues require no es propio del navegador y require un bundler (como en nodejs), por eso instalaremos los babel polyfills.

# Module Loader & Bundlers

Por defecto los navegadores no soportan un cargador de módulos como NodeJS, así que vamos a tener que usar un bundler.

## WebPack

**Webpack** es un sistema de bundling para preparar el desarrollo de una aplicación web para producción.

Con **Webpack** también podemos automatizar procesos de transpilación y preprocesamiento de código como de Sass a Css, de ES6 a ES5, de TS a JS, etc.

1. Para utilizar Webpack debemos tener Node en las maquinas (lo que ya tenemos)
2. instalamos webpack en nuestro equipo o en nuestro proyecto, en este caso lo haremos global
`npm install -g webpack` si queremos hacerlo a nivel de proyecto `npm install --save-dev webpack` y para el proyecto `npm install --save-dev webpack-dev-server`

Los proprocesadores como sass, coffescript y transpiladores como Typescript y Babel, son bienvenidos con webpack mientras le definamos los **__loaders__** para ello.

### Babel + Webpack

Ahora vamos a configurar webpack para que use babel, primero debemos instalar las dependencias de babel restantes:

`npm install --save-dev babel-cli` ya la tenemos instalada así que sólo isntalaremos:

```
npm install --save-dev babel-loader
npm install --save-dev babel-preset-es2015
```
y ahora vamos a configurar el fichero **webpack.config.js**

```
var path = require('path');
var webpack = require('webpack');

module.exports = {
  entry: {
    app: ['/']
  },
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'main.bundle.js'
  },
  resolve: {
    extensions: ['.js']
  },
  devServer:{
    host: '0.0.0.0',
    port: 8080,
    inline: true
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: '/node_modules',
        query: {
          presets: ['es2015']
        }
      }
    ]
  },
  stats: { colors: true}
}
```
#### Bungling

```
resolve: {
    extensions: ['.js']
  }
```

Le indica a webpack que sólo procesará archivos .js para el bundle a generar.

#### Punto de entrada 

```
entry: {
    app: ['/']
}
```
Indica desde dónde iniciará el recorrido del app

#### Punto de salida 

```
output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'main.bundle.js'
}

```
definimos que en la carpeta build se creará un archivo app.js con el fundle final.

#### Loaders 
```
module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: '/node_modules',
        query: {
          presets: ['es2015']
        }
      }
    ]
  },
```
Define los loaders a usar.

# Actualizar el package.json

Ahora debemos actualizar nuestros scripts en el package.json para activar webpack

```
 "scripts": {
    "test": "test",
+   "bbuild": "babel src -d lib",
+   "babel": "babel --presets es2015 js/main.js -o build/main.bundle.js",
+   "start": "webpack-dev-server --progress --colors",
+   "webpack": "webpack"
  },
```
## Actualizar index.html

```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FlexStore</title>
    <link rel="stylesheet" href="./css/styles.css">
    <script src="vendors/jquery/jquery-3.2.1.min.js"></script>
</head>
<body>
    <header>
        <div id="menu">-</div>
        <div id="logo"></div>
        <div class="searchbar">
            <input type="text" placeholder="Buscar...">
            <button>Buscar</button>
        </div>
        <div class="login">Login</div>
    </header>
    <aside>
        <div class="category">
                <span>Categoria</span>
                <div class="item">foo</div>
                <div class="item">foo</div>
                <div class="item">foo</div>
                <div class="item">foo</div>
                <div class="item">foo</div>
                <div class="item">foo</div>
                <div class="item">foo</div>
                <div class="item">foo</div>
        </div>
    </aside>
    <div id="items">
                
    </div>

    
    <script src="store/catalog.js"></script>
+    <script src="build/main.bundle.js"></script>
</body>
</html>

```

# Correr esta belleza

`npm run-script start`