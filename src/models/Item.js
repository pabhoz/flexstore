export class Item{

    constructor(name,thumb){
     this._name = name;
     this._thumb = thumb;
    }

    get name(){
        return this._name;
    }
    set name(name){
        this._name = name;
    }

    get thumb(){
        return this._thumb;
    }
    set thumb(thumb){
        this._thumb = thumb;
    }

}
  