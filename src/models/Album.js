import { Audio } from './Audio.js';

export class Album{
    
        constructor(name,img,desc,score,songs){
            
            this._name = name;
            this._img = img;
            this._desc = desc;
            this._score = score;
            this._songs = [];
            this._DOMElement = null;
            this.initDOMElement();
            this.initSongs(songs);
        }

        initSongs(songs){
            songs.forEach(function(song) {

                let audio = new Audio(song.name,song.src);
                this._songs.push(audio);
    
                this._DOMElement.appendChild(audio.DOMElement);

            }, this);
        }

        initDOMElement(){
            
            let loader = document.createElement("div");
            loader.className = "loader";

            let item = document.createElement("div");
            item.className = "item";
            item.style.background = `url(${this._img})`;
            item.style.backgroundSize = "cover";

            item.appendChild(loader);
            
            this._DOMElement = item;

            this.initListeners();
        }

        initListeners(){
            $(this._DOMElement).mouseover(() => {
                this._songs[0].play();
                 
             })
             .mouseout(()=>{
                 this._songs[0].pause();
             });
        }

        addTo(container){
            container.appendChild(this._DOMElement);
        }
    
}
